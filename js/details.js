const id = new URLSearchParams(window.location.search).get('id');

const deleteButton = document.querySelector('.delete');

const detail = document.querySelector('.details')
const renderDetails = async ()=>{
    const res = await fetch('http://localhost:3000/post/'+id)
    const post = await res.json();
    console.log(post)


    const template = `
    <h1>${post.title}</h1>
    <p>${post.body}</p>
    `


    detail.innerHTML=template
}

deleteButton.addEventListener('click', async (e)=>{
    const res = await fetch(' http://localhost:3000/post/'+ id,{
        method :'DELETE'
    })
    window.location.replace('/index.html')
})
window.addEventListener('DOMContentLoaded',()=> renderDetails())
