const container = document.querySelector('.blogs');

const renderPost = async () =>{
    let uri = 'http://localhost:3000/post?_sort=likes&_order=desc';

    const res = await fetch(uri);
    const post = await res.json();
    console.log(post);

    let template ='';
    post.forEach(p => {
        template += `
            <div class="post">
            <h2>${p.title}</h2>
            <p><small> ${p.likes} likes</small></p>
            <p>${p.body.slice(0,200)}</p>
            <a href="/details.html?id=${p.id}"> read more..</a>
            </div>
        `
    })

    container.innerHTML=template;

}



window.addEventListener('DOMContentLoaded', ()=> renderPost());